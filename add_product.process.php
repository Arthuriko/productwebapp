<?php
require "database_connection.php";
require "class.php";

try{
    if(isset($_POST['submit'])){
        $dropdown = $_POST['dropdown'];
            if($dropdown == "dvd"){
                $dvd_disc = new Dvd_disc($_POST['SKU'], $_POST['product_name'],$_POST['price'],$_POST['size']);
                $dvd_disc->insert_into_products($conn,$dropdown);
            }
            else if($dropdown == "book"){
                $book = new Book($_POST['SKU'], $_POST['product_name'],$_POST['price'],$_POST['weight']);
                $book->insert_into_products($conn,$dropdown);
            }
            else if($dropdown == "furniture"){
                $furniture = new Furniture($_POST['SKU'], $_POST['product_name'],$_POST['price'],$_POST['height'],$_POST['width'],$_POST['length']);
                $furniture->insert_into_products($conn,$dropdown);
            }
        }
}
catch(InvalidArgumentException $e){
    header("Location: ../add_product.php?error=".$e->getMessage());
}
?>
