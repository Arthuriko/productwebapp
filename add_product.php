<!DOCTYPE html>
<html lang="en">

<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script type="text/javascript" src="show_function.js"></script>
  <?php include "database_connection.php";?>
  <?php include "html/header";?>
  <meta charset="UTF-8">
  <title> Test </title>
  <link rel="stylesheet" href= "css/styles.css">

</head>

<body>
  <!-- ----------------------FORM---------------- -->
  <form class="productform" method="post" action ="add_product.process.php">
    <!-- ----------------------SKU---------------- -->
    <div class="field">
      <span>SKU:</span>
      <input type="text" name="SKU">
    </div>

    <!-- ----------------------NAME---------------- -->
    <div class="field">
      <span>Name:</span>
      <input type="text" name="product_name">
    </div>

    <!-- ----------------------PRICE---------------- -->
    <div class="field">
      <span>Price:</span>
      <input type="text" name="price" placeholder="in $">
    </div>

    <!-- ----------------------DROPDOWN---------------- -->
    <div class="field">
      <span>Product Type:</span>  
      <select id="dropdown" name="dropdown">  
        <option value="select">Select</option> 
        <option value="dvd">DVD-disc</option>  
        <option value="book">Book</option>
        <option value="furniture">Furniture</option>
      </select> 
    </div>

    <!-- ----------------------SIZE---------------- -->
    <div class="field" style="display:none" id="sizein">
      <span>Size:<br></span>
      <input type="text" name="size">
      <p class="info">
        Note: Size should be in MB. Can contain 5 digits and 2 after decimal point.
      </p>
    </div>

    <!-- ----------------------WEIGHT---------------- -->
    <div class="field" style="display:none" id="weightin">
      <span>Weight:</span>
      <input type="text" name="weight">
      <p class="info">
        Note: Weight is in KG. Maximum 4 digits and 2 after decimal point.
      </p>
    </div>

    <!-- ----------------------HxWxL---------------- -->
    <div class="field" style="display:none" id="heightin">
      <span>Height:</span>
      <input type="text" name="height">
    </div>
    <div class="field" style="display:none" id="widthin">
      <span>Width:</span>
      <input type="text" name="width">
    </div>
    <div class="field" style="display:none" id="lengthin">
      <span>Length:</span>
      <input type="text" name="length">
      <p class="info">
        Note: HxWxL should be in MM. Maximum 8 digits and 2 after decimal point.
      </p>
    </div>

    <!-- ----------------------Submit---------------- -->
    <input type="submit" class="submit_button" name="submit">
  </form>
  <script type="text/javascript" src="info_messages.js"></script>
</body>
