$(document).ready(function() {
    $("#delete_button").click(function(){
        let products_to_delete = [];
        $.each($("input[id='delete_mark']:checked"),function(){
            products_to_delete.push($(this).siblings("#element_SKU").text());
        })
        
        if (products_to_delete.length) {
            $.ajax({
                type: "POST",
                url: 'delete_array.php',
                dataType: 'json',
                data: {id_list: products_to_delete.join(',')},
            
                success: function (obj, textstatus) {
                    if('error' in obj ) {
                        console.log(obj.error);
                    }
                }
            });
            window.location.reload();
        }
    })
})
    