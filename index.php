<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php include "database_connection.php";?>
    <?php include "html/header";?>
    <?php include "Class.php";?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="delete_objects.process.js"></script>
    <title>Test</title>
    <link rel="stylesheet" href= "css/styles.css">
</head>

<body>

<div class="wrapper">
    <div class="delete">
            <button type="submit" id="delete_button" name="button"> Delete </button>
    </div>


    <div class="content">

    <?php
        $sql = "SELECT * FROM products ORDER BY SKU";
        if($result = $conn->query($sql)){
            while($row = $result->fetch_assoc()){
                if($row["Type"]== "dvd"){  
                    $dvd_disc = new Dvd_disc(   $row["SKU"], $row["Name"],
                                                $row["Price"], $row["Size"]);
                    echo $dvd_disc->create_dvd_html();
                }

                if($row["Type"] == "book"){
                    $book = new Book(   $row["SKU"],$row["Name"],
                                        $row["Price"],$row["Weight"]);
                    echo $book->create_book_html();
                }
                
                if($row["Type"] == "furniture"){
                    $furniture = new Furniture( $row["SKU"],$row["Name"],
                                                $row["Price"],$row["Height"],
                                                $row["Width"],$row["Length"]);
                    echo $furniture->create_furniture_html();
                }
            }
        }
        ?>
    </div>
</div>
</body>