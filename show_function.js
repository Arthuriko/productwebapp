function show_elements(show_this) {
    var all_elements = ["#sizein", "#weightin", "#heightin","#widthin","#lengthin"];
    for (i in all_elements){
        if (show_this.has(all_elements[i])){   
            $(all_elements[i]).show();
        }else{
            $(all_elements[i]).hide();
            $(all_elements[i]+' input').val('');
        }
    }
    return 0;
  }

$(document).ready(function(){
    $('#dropdown').on('change',function(){
        if(this.value =='dvd'){
            show_elements(new Set(["#sizein"]));
        }else if(this.value =='book'){
            show_elements(new Set(["#weightin"]));
        }else if(this.value == 'furniture'){
            show_elements(new Set(["#heightin","#widthin","#lengthin"]));
        }else{
            show_elements(new Set([]));
        }
    })
});
