
# Junior test project

Test project task that contains 2 web pages. Main web page show all products from database with information that bound to product type. Second page contains form that sends products to the database.

*Project contains 18 files including 4 images and readme.md.*

## The following files:

### SQL scripts

* **Create_table.sql**: creates table products in database.

* **Create_fields_for_db.sql**: inserts several product elements into the products table.

### PHP files

* **index.php**: main web page that shows all product elements from database.

* **add_product.php**: web page which provide to add new products to database.

* **add_product.process.php**: checks information from the form and calls the appropriate functions.

* **class.php**: contains classes description, including all fields and methods.

* **database_connection.php**: performs database connection.

* **delete_array.php**: removes all selected products in index.php from database.

### JS files

* **delete_objects.process.js**: definines the SKU number of selected elements and sends an array to delete_array.php.

* **show_function.js**: handles selected type on form page.

* **info_messages.js**: creates alert messages after submitting the form data.

### HTML & CSS files

* **header**: contains markups and text headers for index.php and add_product.php.

* **style.css**: contains all styles for both web pages and their elements.