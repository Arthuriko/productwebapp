INSERT INTO products(SKU, Name, Price, Type, Size, Weight, Height, Width, Length)
VALUES 
        (1,	"Da Vinci Code", 50, "book",NULL,2,NULL,NULL,NULL),
        (2,	"Harry potter and the deathly hallows", 40, "book",NULL,1,NULL,NULL,NULL),
        (3,	"Harry Potter and the Philosopher Stone", 40, "book",NULL,1,NULL,NULL,NULL),
        (4,	"Empty disc 2000", 5, "dvd",2000,NULL,NULL,NULL,NULL),
        (5,	"ULTRA DVD", 10, "dvd",4000,NULL,NULL,NULL,NULL),
        (6,	"DVD-10", 5, "dvd",1000,NULL,NULL,NULL,NULL),
        (7,	"Bed", 200, "furniture",NULL,NULL, 500, 100, 200),
        (8,	"Pillow", 30, "furniture",NULL,NULL, 50, 100, 100),
        (9,	"Ultra Lamp", 200, "furniture",NULL,NULL, 500, 1000, 2000);