<?php
class Product{
    protected $SKU;
    protected $name;
    protected $price;

    protected function create_html(){
        return "<div class='product'>
                <input type='checkbox' id='delete_mark' value='delete_mark'>
                <p id='element_SKU'>$this->SKU </p>
                <p id='element_name'>$this->name</p>
                <p id='element_price'>$this->price $</p>";
    }
}

class Dvd_disc extends Product{
    private $size;

    public function __construct($SKU, $name, $price, $size){
        if(empty($SKU) || empty($name) || empty($price) || empty($size)){
            throw new InvalidArgumentException('empty_fields');
        }
        if(!is_numeric($SKU)|| !is_numeric($price) || !is_numeric($size)){
            throw new InvalidArgumentException('check_fields_for_syntax');
        }
        $this->SKU = $SKU;
        $this->name = $name;
        $this->price = $price;
        $this->size = $size;
    }

    public function insert_into_products($conn, $dropdown){
        $sql = sprintf("INSERT INTO products(SKU, name, price, type, size) 
                            VALUES(%d,'%s',%d, '%s', %d)", $this->SKU, $this->name, $this->price, $dropdown, $this->size);
        if(mysqli_query($conn, $sql)){
            header("Location: ../add_product.php?operation=successful");
        }else{
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        }
        mysqli_close($conn);
    }

    public function create_dvd_html(){
        return $this->create_html() .   
                            "<p id='element_size'>Size:$this->size MB</p>
                            <img id='img_dvd' src='/css/images/dvd.png'>
                            </div>";
    }
}

class Book extends Product{
    private $weight;

    public function __construct($SKU, $name, $price, $weight){
        if (empty($SKU) || empty($name) || empty($price) || empty($weight)){
            throw new InvalidArgumentException('empty_fields'); 
        }
        if(!is_numeric($SKU)|| !is_numeric($price) || !is_numeric($weight)){
            throw new InvalidArgumentException('check_fields_for_syntax');
        }
        else{
        $this->SKU = $SKU;
        $this->name = $name;
        $this->price = $price;
        $this->weight = $weight;
        }
    }

    public function insert_into_products($conn, $dropdown){
        $sql = sprintf("INSERT INTO products(SKU, name, price, type, weight) 
                            VALUES(%d, '%s', %d, '%s', %d)", $this->SKU, $this->name, $this->price, $dropdown, $this->weight);
        if(mysqli_query($conn, $sql)){
            header("Location: ../add_product.php?operation=successful");
        }else{
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        }
        mysqli_close($conn);
    }

    public function create_book_html(){
        return $this->create_html() .   
                            "<p id='element_weight'>Weight:$this->weight KG</p>
                            <img id='img_book' src='/css/images/book.png'>
                            </div>";
    }
}

class Furniture extends Product{
    private $height;
    private $width;
    private $length;

    public function __construct($SKU, $name, $price, $height, $width, $length){
        if (empty($SKU) || empty($name) || empty($price) || empty($height) || empty($width) || empty($length)){
            throw new InvalidArgumentException('empty_fields'); 
        }
        else if(!is_numeric($SKU)|| !is_numeric($price) || !is_numeric($height) || !is_numeric($width) || !is_numeric($length)){
            throw new InvalidArgumentException('check_fields_for_syntax');
        }
        else{
        $this->SKU = $SKU;
        $this->name = $name;
        $this->price = $price;
        $this->height = $height;
        $this->width = $width;
        $this->length = $length;
        }
    }

    public function insert_into_products($conn, $dropdown){
        $sql = sprintf("INSERT INTO products(SKU, name, price, type, height, width, length)
                    VALUES(%d,'%s', %d, '%s', %d, %d, %d)",
                    $this->SKU, $this->name, $this->price, $dropdown, $this->height, $this->width, $this->length);
        if(mysqli_query($conn, $sql)){
            header("Location: ../add_product.php?operation=successful");
        }else{
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        }
        mysqli_close($conn);
    }

    public function create_furniture_html(){
        return $this->create_html() .   
                            "<p id='element_HxWxL'>HxWxL:" . $this->height . "x" . $this->width . "x" . $this->length . " MM</p>
                            <img id='img_furniture' src='/css/images/furniture.jpg'>
                            </div>";
    }
}
?>