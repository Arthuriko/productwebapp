CREATE TABLE products(
    SKU INT(8) PRIMARY KEY,
    Name CHAR(20) NOT NULL,
    Price DECIMAL(8,2) NOT NULL,
    Weight DECIMAL(4,2) NOT NULL,
    Size DECIMAL(7,2) NOT NULL,
    Height INT(6) NOT NULL,
    Width INT(6) NOT NULL,
    Length INT(6) NOT NULL,
    Type text NOT NULL);